<?php

require_once 'Controleur/Controleur.php';
require_once 'Vue/Vue.php';
require_once 'Modele/Produit.php';

class ControleurProduitList implements Controleur
{
    /**
     * @var Produit
     */
    private $produit;


    public function __construct()
    {
        $this->produit = new Produit();
    }

    /**
     * Getter du produit
     *
     * @return Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Setter du produit
     *
     * @param $newProduct
     */
    public function setProduit($newProduct)
    {
        $this->produit = $newProduct;
    }


   
    public function getHTML()
    {
        $vue = new Vue("ProduitList");
        $vue->generer(array('ProduitList' => $this->produit->getAllProduit()));
    }


}
