<?php


require_once 'Controleur/Controleur.php';
require_once 'Vue/Vue.php';
require_once 'Modele/AdministrationPaiementLivraison.php';

class ControleurAdministrationPaiementLivraison implements Controleur
{
   
    /**
     * @var AdministrationPaiementLivraison
     */
    private $adminPaiementLivraison;
    /**
     * @var int
     */
    private $adminPaiementLivraison_code;


   
    public function __construct()
    {
        $this->adminPaiementLivraison = new AdministrationPaiementLivraison();
        $this->adminPaiementLivraison_code = 0; // default
    }

    /**
     * Getter de $adminPaiementLivraison
     *
     * @return AdministrationPaiementLivraison
     */
    public function getAdminPaiementLivraison()
    {
        return $this->adminPaiementLivraison;
    }

    /**
     * Getter de $adminPaiementLivraison_code
     *
     * @return int adminPaiementLivraison_code
     */
    public function getAdminPaiementLivraison_code()
    {
        return $this->adminPaiementLivraison_code;
    }

    /**
     * Setter de $adminPaiementLivraison
     *
     * @param $newAdminPaiementLivraison
     */
    public function setAdminPaiementLivraison($newAdminPaiementLivraison)
    {
        $this->adminPaiementLivraison = $newAdminPaiementLivraison;
    }

    /**
     * Setter de $adminPaiementLivraison_code
     *
     * @param $newAdminPaiementLivraison_code
     */
    public function setAdminPaiementLivraison_code($newAdminPaiementLivraison_code)
    {
        $this->adminPaiementLivraison_code = $newAdminPaiementLivraison_code;
    }

  
    public function handlerPaiementLivraison()
    {
        if (isset($_SESSION['userID']) && $_SESSION['niveau_accreditation'] == 1) {
            $this->addPaiementLivraison(); 
            $this->checkEditPaiement();  
            $this->removePaiementLivraison();  

            $this->getHTML();
        } else {
            header('Location: index.php?action=login');
            die();
        }
    }


  
    private function addPaiementLivraison()
    {
        if (empty($_POST['nomPaiementLivraison']) && empty($_POST['descriptionPaiementLivraison']))
            $this->adminPaiementLivraison_code = 0;
        elseif (empty($_POST['nomPaiementLivraison']) || empty($_POST['descriptionPaiementLivraison']))
            $this->adminPaiementLivraison_code = AdministrationPaiementLivraison::MISSING_PARAMETERS;
        elseif (empty($_GET['do']) && !empty($_POST['nomPaiementLivraison']) && !empty($_POST['descriptionPaiementLivraison'])) {
            $this->adminPaiementLivraison_code = $this->adminPaiementLivraison->insertPaiementLivraison($_POST['nomPaiementLivraison'], $_POST['descriptionPaiementLivraison']);
        }
    }


    private function checkEditPaiement()
    {
        if (!empty($_GET['do']) && !empty($_GET['paiementID'])) {
            if ($_GET['do'] == "delete")
                return;
            if ($_GET['do'] == "editPaiement") {
                $this->adminPaiementLivraison->editMoyenPaiement($_POST['nomPaiementLivraison'], $_POST['descriptionPaiementLivraison'], $_GET['paiementID']);
                $this->adminPaiementLivraison_code = AdministrationPaiementLivraison::EDIT_OK;
            }
        }
        if (!empty($_GET['paiementID']) && empty($_GET['do'])) {
            $paiement = $this->adminPaiementLivraison->getMoyensPaiementById($_GET['paiementID']);
            $vue = new Vue("AdminEditPaiement");
            $vue->generer(array(
                'paiement' => $paiement,
                'listMoyensPaiement' => $this->adminPaiementLivraison->getMoyensPaiement()));
            die();
        }
    }


  
    private function removePaiementLivraison()
    {
        if (!empty($_GET['paiementID']) && !empty($_GET['do'])) {
            if ($_GET['do'] == "delete")
                $this->adminPaiementLivraison_code == $this->adminPaiementLivraison->removePaiementLivraison($_GET['paiementID']);
        }
    }


   
    public function getHTML()
    {
        
        $vue = new Vue("AdministrationPaiementLivraison");
       
        $vue->generer(array(
            'listMoyensPaiement' => $this->adminPaiementLivraison->getMoyensPaiement(),
            'code' => $this->adminPaiementLivraison_code
        ));

    }

}

?>
